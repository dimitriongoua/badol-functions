require('dotenv').config();

const admin = require('firebase-admin');

const serviceAccount = JSON.parse(process.env.SERVICE_ACCOUNT);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.DATABASE_URL
});

const db = admin.firestore();

exports.firestore = admin.firestore;

exports.s = async function (collection) {

    const snapshot = await db.collection(collection).get();
    if (snapshot.empty) {
        console.log(`No documents found for "${collection}" collection.`);
        return [];
    } else {
        let docs = [];
        snapshot.docs.forEach(doc => {
            docs.push(doc.data());
        });
        return docs;
    }
}

exports.one = async function (collection, params) {

    const champ = Object.keys(params)[0];
    const valeur = params[champ];

    const ref = db.collection(collection);
    const snapshot = await ref.where(champ, '==', valeur).get();
    if (snapshot.empty) {
        return null;
    } else {
        let id = snapshot.docs[0].id;
        let item = snapshot.docs[0].data();
        item.id = id;
        return item;
    }
}

exports.e = async function (collection, doc) {

    console.log(`Enregistrment ...`);
    console.log(doc);
    if (doc.id) {
        return await db.collection(collection).doc(doc.id).set(doc);
    } else {
        let res = await db.collection(collection).add(doc);
        return res.id;
    }
}

exports.reponse = (data) => {
    const output = { error: false, data: data };
    return {
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(output)
    }
}

exports.erreur = (message) => {
    const output = { error: true, message: message };
    return {
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(output)
    }
}