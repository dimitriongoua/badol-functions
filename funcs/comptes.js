const { s, e, reponse, erreur, firestore } = require('../utils');

exports.handler = async (event, context) => {

    switch (event.httpMethod) {
        case "GET":
            let comptes = await s("comptes");
            return reponse(comptes);
        case "POST":
            let params = JSON.parse(event.body);
            try {
                let compte = {
                    numero: params.numero,
                    pass: params.pass,
                    solde: params.solde,
                    date_creation: firestore.FieldValue.serverTimestamp()
                }

                let res = await e("comptes", compte);
                return reponse({id: res});
            } catch (error) {
                return erreur(error);
            }
        default:
            return erreur("Method Not Allowed");
    }
}