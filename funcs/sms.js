require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

exports.handler = (event, context, callback) => {
    if (event.httpMethod !== "POST") {
        callback(null, {
            statusCode: 200,
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ erreur: true, message: "Method Not Allowed" })
        });
    } else {
        let params = JSON.parse(event.body);
        client.messages
            .create({
                body: params.message,
                from: process.env.TWILIO_FROM,
                to: params.numero
            })
            .then(message => {
                console.log(message);
                callback(null, {
                    statusCode: 200,
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({ erreur: false, message: `Message envoyé au ${params.numero}` })
                });
            })
            .done();
    }
}

