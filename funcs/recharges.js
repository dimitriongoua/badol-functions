const { s, e, one, reponse, erreur, firestore } = require('../utils');

exports.handler = async (event, context) => {

    switch (event.httpMethod) {
        case "GET":
            let recharges = await s("recharges");
            return reponse(recharges);
        case "POST":
            let params = JSON.parse(event.body);
            try {
                let recharge = {
                    reference: params.reference,
                    montant: params.montant,
                    numero: params.numero,
                    timestamp: params.timestamp,
                    date_creation: firestore.FieldValue.serverTimestamp()
                }

                let rep = await one("recharges", { reference: recharge.reference });

                if (rep) {
                    return erreur("Une recharge avec cette référence existe déjà.");
                } else {
                    rep = await e("recharges", recharge);
                    if (rep) {
                        let compte = await one("comptes", { numero: recharge.numero });
                        if (compte) {
                            // On crédite le compte
                            compte.solde = +compte.solde + +recharge.montant;
                            rep = await e("comptes", compte);
                            if (rep) {
                                return reponse({ message: `Le compte ${compte.numero} a bien été rechargé de ${recharge.montant}`, compte: { numero: compte.numero, solde: compte.solde } });
                            } else {
                                return erreur(`Impossible de créditer le compte ${compte.numero}.`);
                            }
                        } else {
                            // On crée le compte
                            const pass = Math.floor(Math.random() * 9999);
                            const newCompte = {
                                numero: recharge.numero,
                                solde: montant.montant,
                                pass: pass
                            };
                            rep = await e("comptes", newCompte);
                            if (rep) {
                                return reponse({ numero: newCompte.numero, pass: newCompte.pass })
                            } else {
                                return erreur("Une erreur s'est produite lors de la création du compte.");
                            }
                        }
                    } else {
                        return erreur("Une erreur s'est produite lors de l'ajout de la recharge.");
                    }
                }
            } catch (error) {
                console.log(error);
                return erreur(error.message);
            }
        default:
            return erreur("Method Not Allowed");
    }
}